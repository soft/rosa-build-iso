# "@PLATFORM@" is e.g. "rosa2023.1"

# e2kv4

repo --name=Main --baseurl=@REPO_BASE_URL@/@PLATFORM@/repository/e2kv4/main/release
repo --name=Main-Testing --baseurl=@REPO_BASE_URL@/@PLATFORM@/repository/e2kv4/main/testing
repo --name=Contrib --baseurl=@REPO_BASE_URL@/@PLATFORM@/repository/e2kv4/contrib/release
repo --name=Contrib-Testing --baseurl=@REPO_BASE_URL@/@PLATFORM@/repository/e2kv4/contrib/testing
repo --name=Non-Free --baseurl=@REPO_BASE_URL@/@PLATFORM@/repository/e2kv4/non-free/release
repo --name=Non-Free-Testing --baseurl=@REPO_BASE_URL@/@PLATFORM@/repository/e2kv4/non-free/testing

