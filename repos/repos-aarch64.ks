# "@PLATFORM@" is e.g. "rosa2023.1"

repo --name=Mainaa --baseurl=@REPO_BASE_URL@/@PLATFORM@/repository/aarch64/main/release
repo --name=Mainaa-Testing --baseurl=@REPO_BASE_URL@/@PLATFORM@/repository/aarch64/main/testing
repo --name=Contribaa --baseurl=@REPO_BASE_URL@/@PLATFORM@/repository/aarch64/contrib/release
repo --name=Contribaa-Testing --baseurl=@REPO_BASE_URL@/@PLATFORM@/repository/aarch64/contrib/testing
repo --name=Non-Freeaa --baseurl=@REPO_BASE_URL@/@PLATFORM@/repository/aarch64/non-free/release
repo --name=Non-Freeaa-Testing --baseurl=@REPO_BASE_URL@/@PLATFORM@/repository/aarch64/non-free/testing
