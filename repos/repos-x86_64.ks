# "@PLATFORM@" is e.g. "rosa2023.1"

repo --name=Main --baseurl=@REPO_BASE_URL@/@PLATFORM@/repository/x86_64/main/release
repo --name=Main-Testing --baseurl=@REPO_BASE_URL@/@PLATFORM@/repository/x86_64/main/testing
repo --name=Contrib --baseurl=@REPO_BASE_URL@/@PLATFORM@/repository/x86_64/contrib/release
repo --name=Contrib-Testing --baseurl=@REPO_BASE_URL@/@PLATFORM@/repository/x86_64/contrib/testing
repo --name=Non-Free --baseurl=@REPO_BASE_URL@/@PLATFORM@/repository/x86_64/non-free/release
repo --name=Non-Free-Testing --baseurl=@REPO_BASE_URL@/@PLATFORM@/repository/x86_64/non-free/testing
