# "@PLATFORM@" is e.g. "rosa2023.1"

repo --name=Main32 --baseurl=@REPO_BASE_URL@/@PLATFORM@/repository/i686/main/release
repo --name=Main32-Testing --baseurl=@REPO_BASE_URL@/@PLATFORM@/repository/i686/main/testing
repo --name=Contrib32 --baseurl=@REPO_BASE_URL@/@PLATFORM@/repository/i686/contrib/release
repo --name=Contrib32-Testing --baseurl=@REPO_BASE_URL@/@PLATFORM@/repository/i686/contrib/testing
repo --name=Non-Free32 --baseurl=@REPO_BASE_URL@/@PLATFORM@/repository/i686/non-free/release
repo --name=Non-Free32-Testing --baseurl=@REPO_BASE_URL@/@PLATFORM@/repository/i686/non-free/testing
