# скопировать в образ скрипты rosa-build-iso
# в /var/lib/offlinerebuilder/rosa-build-iso
mkdir -p $INSTALL_ROOT/var/lib/offlinerebuilder/rosa-build-iso
rsync -av "$DIR0"/ $INSTALL_ROOT/var/lib/offlinerebuilder/rosa-build-iso

# /root/.ssh/authorized_keys создается в c/f/ssh-server
cp -rv $INSTALL_ROOT/root/.ssh $INSTALL_ROOT/var/lib/offlinerebuilder

chroot $INSTALL_ROOT /bin/chown -R offlinerebuilder:offlinerebuilder /var/lib/offlinerebuilder
