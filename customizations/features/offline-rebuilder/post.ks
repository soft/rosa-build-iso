echo offline-rebuilder > /etc/hostname
touch /etc/.nogenhostname

# Anaconda и Grub2 берут название продукта из /etc/system-release
. /etc/os-release
echo ROSA Offline Rebuilder $VERSION release $VERSION_ID for $(uname -m) > /etc/system-release

useradd --uid 501 --create-home --home-dir /var/lib/offlinerebuilder offlinerebuilder
usermod -a -G wheel offlinerebuilder
passwd -d offlinerebuilder
echo "offlinerebuilder ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers

cat > /etc/anaconda-scripts.d/post-install/20-autologin.sh << 'EOF'
#!/bin/bash
if [ -f /etc/gdm/custom.conf ]; then
	py-ini-config set /etc/gdm/custom.conf daemon AutomaticLoginEnable true
	py-ini-config set /etc/gdm/custom.conf daemon AutomaticLogin offlinerebuilder
fi
EOF
chmod 755 /etc/anaconda-scripts.d/post-install/20-autologin.sh

cat > /etc/anaconda-scripts.d/livecd-init/20-anaconda-conf.sh << 'EOF'
f=/etc/anaconda/conf.d/10-offline-rebuilder.conf
# отключаем споук создания пользователей в анаконде (входить под offlinerebuilder)
echo '[User Interface]' > "$f"
echo 'hidden_spokes = UserSpoke' >> "$f"
# Такая схема разбивки сделает 1 раздел btrfs, в котором
# будут созданы подтома home и root, а дисковое пространство будет общим,
# то есть не разделяем пространство на / и /home
echo '[Storage]' >> "$f"
echo 'default_scheme = BTRFS' >> "$f"
EOF
chmod +x /etc/anaconda-scripts.d/livecd-init/20-anaconda-conf.sh

# Создадим директории, куда предполагается монтировать ISO,
# согласно коду offline_rebuild.sh
mkdir -p /mnt/repo-srpms
mkdir -p /mnt/repo

# Отключаем службу поиска KDE, чтобы не жрала ресурсы на индексирование
cat > /etc/xdg/baloofilerc << 'EOF'
[Basic Settings]
Indexing-Enabled=false
EOF
