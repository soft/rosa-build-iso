systemctl disable systemd-resolved
systemctl mask systemd-resolved
sed -i'' -e 's,resolve \[\!UNAVAIL=return\],,' /etc/nsswitch.conf
# symlink to systemd-resolved in /run/
rm -f /etc/resolv.conf
touch /etc/resolv.conf
