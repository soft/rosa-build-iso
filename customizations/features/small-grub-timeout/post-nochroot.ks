# ROSA-specific patches in livecd-tools create grub.cfg and lang.cfg
find "$INSTALL_ROOT"/.. '(' -name grub.cfg -o -name lang.cfg ')' | xargs sed -i'' -e 's,timeout=10,timeout=1,g'
