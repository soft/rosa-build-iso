# Assume that web-server is run by `python3 -m http.server` as suggested in http://wiki.rosalab.ru/ru/index.php/Anaconda;
# change the URL if needed.
bootloader --append="plymouth.enable=0 systemd.unit=anaconda.target inst.text inst.ks=http://192.168.122.1:8000/anaconda.ks"
