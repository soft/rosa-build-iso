cat > /usr/local/bin/autosetup.sh << 'EOF'
%include features/freeipa/autosetup.sh
EOF
chmod 755 /usr/local/bin/autosetup.sh

cat > /etc/systemd/system/autosetup.service << 'EOF'
%include features/freeipa/autosetup.service
EOF

systemctl enable autosetup.service

echo dc1.ipa.loc > /etc/hostname
touch /etc/.nogenhostname
