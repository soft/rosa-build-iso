#!/bin/bash
# This script starts a FreeIPA domain controller
set -efu

ip="$(hostname -I | tr ' ' '\n' | head -n 1)"
echo "$ip $(hostname) $(hostname --short)" >> /etc/hosts

ipa-server-install \
	--realm="$(hostname --domain | tr '[:lower:]' '[:upper:]')" \
	--ds-password=IpaDs2022 \
	--admin-password=IpaAdmin2022 \
	--setup-dns \
	--forwarder="127.0.0.53" \
	--unattended
