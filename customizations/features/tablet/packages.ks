# hacks for rotating screen and other things
tablet-rotation
# tablet rotation "Recommends" onboard and touchegg
onboard
touchegg

# easy test of microphone
audio-recorder

# install cheese, do not intall kamoso if
# it is pulled from another list or task-iso* meta-package
# (kamoso is too buggy)
-kamoso
cheese

# GUI for mobile broadband modems
# (e.g. run USSD commands to get balance)
modem-manager-gui
