# TODO: package this like rosa-xfce-config-gdm and gdm-default-to-xorg?
# Note: https://bugzilla.rosalinux.ru/show_bug.cgi?id=13239

if rpm -q gdm; then
mkdir -p /etc/systemd/system/gdm.service.d/
cat > /etc/systemd/system/gdm.service.d/override.conf << 'EOF'
[Service]
# Wayland works better than Xorg on Baikal-M with Mali GPU
Environment=GDM_DEFAULT_SESSION=plasmawayland
EOF
fi

if rpm -q sddm; then
echo "[Last]" > /var/lib/sddm/state.conf
echo "Session=/usr/share/wayland-sessions/plasmawayland.desktop" >> /var/lib/sddm/state.conf
py-ini-config set /etc/sddm.conf General DisplayServer wayland
# DE-specific post.ks already configures DMs
cat >> /etc/anaconda-scripts.d/livecd-init/20-wayland.sh << 'EOF'
#!/bin/bash
for i in /etc/sddm.conf /etc/sddm.conf.d/99-autologin-live.conf
do
	if test -f "$i" && grep -q "^Session=" "$i"; then
		py-ini-config set "$i" Autologin Session plasmawayland.desktop
	fi
done
EOF
chmod +x /etc/anaconda-scripts.d/livecd-init/20-wayland.sh
fi
