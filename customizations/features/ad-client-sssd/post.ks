echo client1-sssd.samba.loc > /etc/hostname
touch /etc/.nogenhostname

# TODO: automate setting IP address
cat > /etc/resolv.conf << EOF
nameserver @IP_of_dc1.samba.loc@
search samba.loc
EOF

# NM will edit /etc/resolv.conf
systemctl disable NetworkManager
systemctl enable dhclient@eth0
cat > /etc/dhclient-enter-hooks.d/nodnsupdate << 'EOF'
make_resolv_conf(){ : ;}
EOF
