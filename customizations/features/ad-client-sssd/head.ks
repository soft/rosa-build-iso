# Override values from DEs/server to avoid running Anaconda
# "net.ifnames=0" makes network interface always be named "eth0" istead on enp1s0 etc.
bootloader --append="systemd.unit=multi-user.target net.ifnames=0"
