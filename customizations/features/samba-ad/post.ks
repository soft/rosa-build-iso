# http://wiki.rosalab.ru/ru/index.php/Samba

cat > /usr/local/bin/autosetup.sh << 'EOF'
%include features/samba-ad/autosetup.sh
EOF
chmod 755 /usr/local/bin/autosetup.sh

cat > /etc/systemd/system/autosetup.service << 'EOF'
%include features/samba-ad/autosetup.service
EOF

systemctl enable autosetup.service

systemctl disable smb nmb winbind

echo dc1.samba.loc > /etc/hostname
touch /etc/.nogenhostname
