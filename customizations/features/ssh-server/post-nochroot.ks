set -xefu

tmpdir="$( umask 077 && mktemp -d )"
ssh-keygen -f "$tmpdir"/sshkey -q -N ""
( umask 077 && mkdir -p "$INSTALL_ROOT"/root/.ssh )
cat "$tmpdir"/sshkey.pub > "$INSTALL_ROOT"/root/.ssh/authorized_keys
mv "$tmpdir"/sshkey "$RESULTS_DIR"/sshkey_"$BUILD_ID"
mv "$tmpdir"/sshkey.pub "$RESULTS_DIR"/sshkey_"$BUILD_ID".pub

# To add your own public SSH keys
cat >> "$INSTALL_ROOT"/root/.ssh/authorized_keys << EOF
%include features/ssh-server/authorized_keys
EOF

# allow root login
if [ -f "$INSTALL_ROOT"/etc/ssh/denyusers ]; then
	echo > "$INSTALL_ROOT"/etc/ssh/denyusers
fi
