systemctl enable sshd

# Make host keys be the same on each run of such LiveCD,
# otherwise ssh client will warn that they have changed
sshd-keygen
