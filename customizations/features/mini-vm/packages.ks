# exclude packages not needed in a special-purpose minimalistic virtual machine

-linux-firmware
-anaconda
-initial-setup
-luksunlock
# XXX anaconda-dracut is needed only because it is added into dracut configs
anaconda-dracut
