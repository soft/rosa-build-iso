cat > /etc/bashrc.add << 'EOF'
%include features/s4/tmux-shellrc
EOF

if test -f /etc/bashrc; then
	# Вставляем содержимое файла tmux-shellrc в конфиг bash после "umask 022"
	# Не удаляем и не создаем заново /etc/bashrc, чтобы случайно не потерять xattr и пр.
	if grep -q '^umask ' /etc/bashrc
	then
		sed '/^umask /q' /etc/bashrc > /etc/bashrc.tmp
		n="$(cat /etc/bashrc.tmp | wc -l)"
		n=$((n+1))
		echo >> /etc/bashrc.tmp
		cat /etc/bashrc.add >> /etc/bashrc.tmp
		echo >> /etc/bashrc.tmp
		tail -n +"$n" /etc/bashrc >> /etc/bashrc.tmp
		cat /etc/bashrc.tmp > /etc/bashrc
		unlink /etc/bashrc.tmp
	else
		echo "ERROR, check script editing /etc/bashrc!"
	fi
fi

unlink /etc/bashrc.add

cat > /etc/tmux.conf << 'EOF'
%include features/s4/tmux.conf
EOF
