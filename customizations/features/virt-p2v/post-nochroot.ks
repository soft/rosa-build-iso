set -x

dir="$(dirname "$(find "$INSTALL_ROOT"/.. -name grub.cfg | head -n1)")"

# ROSA-specific patches in livecd-tools create grub.cfg and lang.cfg

# Delete odd languages from LiveCD Grub menu
sed -i'' "$dir"/lang.cfg \
	-e 's,it_IT=italiano,,' \
	-e 's,it_IT=Italiano,,' 
	-e 's,de_DE=Deutsch,,' \
	-e 's,fr_FR=Français,,' \
	-e 's,pt_PT=Português,,' \
	-e 's,es_ES=Español,,'

msgunfmt -o "$dir"/locale/ru_RU.po "$dir"/locale/ru_RU.mo
rm -fv "$dir"/locale/*.mo

sed -i'' "$dir"/grub.cfg \
	-e 's,Install,Run,g' \
	-e 's,set OS=$"ROSA linux",set OS="ROSA virt-p2v",' \
	-e 's,systemd.unit=anaconda.target,,g'

cat >> "$dir"/locale/ru_RU.po << 'EOF'
msgid "Run"
msgstr "Запустить"
EOF

msgfmt -o "$dir"/locale/ru_RU.mo "$dir"/locale/ru_RU.po
unlink "$dir"/locale/ru_RU.po
