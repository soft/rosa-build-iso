virt-p2v-live-gui
task-x11
gparted
xfce4-terminal
# offed by default
openssh-server

# based on c/f/mini-vm, but must work in VMs and on bare metal
-anaconda
-oem-install
-initial-setup
-luksunlock
# XXX anaconda-dracut is needed only because it is added into dracut configs
anaconda-dracut
