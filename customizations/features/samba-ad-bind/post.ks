# BIND is configured to listen only on localhost, so let's change it to listen on all interfaces
sed -i'' '/listen-on\|allow-query/ s|{.\+}|{ any; }|' /etc/named.conf
