# Ensure that Plymouth is disabled
# Start Anaconda installer (http://wiki.rosalab.ru/ru/index.php/Anaconda)
bootloader --append="plymouth.enable=0 systemd.unit=anaconda.target"

services --enabled=sshd

part / --size 10000 --fstype ext4
