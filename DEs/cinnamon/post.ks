cat >> /etc/anaconda-scripts.d/livecd-init/11-cinnamon.sh << 'EOF'
#!/bin/bash
# Autologin gdm
echo "[daemon]
AutomaticLoginEnable=True
AutomaticLogin=live" > /etc/gdm/custom.conf
# Anaconda mate desktop
sed -i ''s/ROSA Linux/ROSA Linux Cinnamon/' /home/live/Desktop/anaconda-rosa.desktop /usr/share/applications/anaconda-rosa.desktop
EOF

chmod +x /etc/anaconda-scripts.d/livecd-init/11-cinnamon.sh
