cat >> /etc/anaconda-scripts.d/livecd-init/11-gnome.sh << 'EOF'
#!/bin/bash
# Autologin gdm
if [ -f /etc/gdm/custom.conf ]; then
echo "[daemon]
AutomaticLoginEnable=True
AutomaticLogin=live" > /etc/gdm/custom.conf
fi
# Anaconda gnome desktop
sed -i 's/ROSA Linux/ROSA Linux GNOME/' /home/live/Desktop/anaconda-rosa.desktop /usr/share/applications/anaconda-rosa.desktop
EOF
chmod +x /etc/anaconda-scripts.d/livecd-init/11-gnome.sh
