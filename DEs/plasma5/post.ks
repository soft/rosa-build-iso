cat >> /etc/anaconda-scripts.d/livecd-init/11-plasma5.sh << 'EOF'
#!/bin/bash
# Autologin sddm
if [ -f /etc/sddm.conf ]; then
# https://bugzilla.rosalinux.ru/show_bug.cgi?id=13239
session="$(ls /usr/share/xsessions | grep plasma.desktop$ | head -n1)"
# User= in /etc/sddm.conf for rosa2021.1, file in sddm.conf.d for rosa2023.1
# User= and Session= are commented by default in rosa2023.1, sed will do nothing
# /etc/sddm.conf.d/99-autologin-live.conf will do nothing in rosa2021.1, but will work in rosa2023.1
sed -i'' -e "s/^User=.*/User=live/" -e "s/^Session=.*/Session=${session}/" /etc/sddm.conf
echo -e "[Autologin]\nSession=${session}\nUser=live" > /etc/sddm.conf.d/99-autologin-live.conf
fi
# Autologin gdm
if [ -f /etc/gdm/custom.conf ]; then
echo "[daemon]
AutomaticLoginEnable=True
AutomaticLogin=live" > /etc/gdm/custom.conf
fi
# Plasma in gdm
rm -f /usr/share/xsessions/openbox*.desktop
# Anaconda plasma5 desktop
sed -i 's/ROSA Linux/ROSA Linux Plasma/' /home/live/Desktop/anaconda-rosa.desktop /usr/share/applications/anaconda-rosa.desktop
EOF
chmod +x /etc/anaconda-scripts.d/livecd-init/11-plasma5.sh
