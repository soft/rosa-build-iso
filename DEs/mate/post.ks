cat >> /etc/anaconda-scripts.d/livecd-init/11-mate.sh << 'EOF'
# Autologin lightdm
[ -f /etc/lightdm/lightdm.conf ] && sed -i	\
	-e "/pam-service=/ s/.*/pam-service=lightdm/"					\
	-e "/pam-autologin-service=/ s/.*/pam-autologin-service=lightdm-autologin/"	\
	-e "/autologin-user=/ s/.*/autologin-user=live/"				\
	-e "/autologin-user-timeout=/ s/.*/autologin-user-timeout=0/"			\
	-e "/user-session=/ s/.*/user-session=mate/"					\
	/etc/lightdm/lightdm.conf
# Anaconda MATE desktop
sed -i 's/ROSA Linux/ROSA Linux MATE/' /home/live/Desktop/anaconda-rosa.desktop /usr/share/applications/anaconda-rosa.desktop
EOF
chmod +x /etc/anaconda-scripts.d/livecd-init/11-mate.sh

mkdir -p /etc/anaconda-scripts.d/post-install
cat >> /etc/anaconda-scripts.d/post-install/11-mate.sh << 'EOF'
#!/bin/bash
rm -f /etc/skel/.local/share/gvfs-metadata/home
[ -f /etc/lightdm/lightdm.conf ] && sed -i "/user-session=/ s/.*/user-session=mate/" /etc/lightdm/lightdm.conf
EOF
chmod +x /etc/anaconda-scripts.d/post-install/11-mate.sh

