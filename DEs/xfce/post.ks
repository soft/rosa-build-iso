# https://help.gnome.org/admin/gdm/stable/configuration.html.en#sessionconfig
# https://help.gnome.org/admin/system-admin-guide/stable/login-automatic.html.en
# Using TimedLogin, but not AutomaticLogin, in qemu, because spice-vdagent does not
# resize screen in XFCE (XFCE does not support a needed protocol), but GDM does it;
# so we start GDM, it will resize the screen and start XFCE with a correct resolution.
cat > /etc/anaconda-scripts.d/livecd-init/rosa-xfce-gdm-setup.sh << 'EOF'
if test -e /dev/virtio-ports/com.redhat.spice.0
	then type=Timed
	else type=Automatic
fi
echo "[daemon]
${type}LoginEnable=true
${type}Login=live
TimedLoginDelay=1
WaylandEnable=false" > /etc/gdm/custom.conf
EOF
chmod +x /etc/anaconda-scripts.d/livecd-init/rosa-xfce-gdm-setup.sh

( cd /etc/systemd/system && ln -sf gdm.service display-manager.service )
