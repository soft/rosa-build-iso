# Disable the issue of trusting the installer LXQt
/bin/mkdir -p /etc/skel/.local/share/gvfs-metadata
/bin/echo \
'00000000: da1a 6d65 7461 0100 0000 0000 d9ff 9b77  ..meta.........w
00000010: 0000 0030 0000 0020 0000 0000 6045 2b4d  ...0... ....`E+M
00000020: 0000 0001 0000 0028 7472 7573 7400 0000  .......(trust...
00000030: 0000 0040 0000 0044 0000 0090 a1e4 2571  ...@...D......%q
00000040: 2f00 0000 0000 0001 0000 0058 0000 0060  /..........X...`
00000050: 0000 0094 0000 0001 4465 736b 746f 7000  ........Desktop.
00000060: 0000 0001 0000 0074 0000 008c 0000 0098  .......t........
00000070: 0000 05cd 616e 6163 6f6e 6461 2d72 6f73  ....anaconda-ros
00000080: 612e 6465 736b 746f 7000 0000 0000 0000  a.desktop.......
00000090: 0000 0000 0000 0000 0000 0001 0000 0000  ................
000000a0: 0000 00a4 7472 7565 0000 0000            ....true....' \
	|/usr/bin/xxd -r > /etc/skel/.local/share/gvfs-metadata/home

cat >> /etc/anaconda-scripts.d/livecd-init/11-lxqt.sh << 'EOF'
#!/bin/bash
# Autologin gdm
echo "[daemon]
AutomaticLoginEnable=True
AutomaticLogin=live" > /etc/gdm/custom.conf
# Autologin lightdm
[ -f /etc/lightdm/lightdm.conf ] && sed -i	\
	-e "/pam-service=/ s/.*/pam-service=lightdm/"					\
	-e "/pam-autologin-service=/ s/.*/pam-autologin-service=lightdm-autologin/"	\
	-e "/autologin-user=/ s/.*/autologin-user=live/"				\
	-e "/autologin-user-timeout=/ s/.*/autologin-user-timeout=0/"			\
	-e "/user-session=/ s/.*/user-session=lxqt/"					\
	/etc/lightdm/lightdm.conf
# Anaconda LXQt desktop
sed -i 's/ROSA Linux/ROSA Linux LXQt/' /home/live/Desktop/anaconda-rosa.desktop /usr/share/applications/anaconda-rosa.desktop
EOF
chmod +x /etc/anaconda-scripts.d/livecd-init/11-lxqt.sh

mkdir -p /etc/anaconda-scripts.d/post-install
cat >> /etc/anaconda-scripts.d/post-install/11-lxqt.sh << 'EOF'
#!/bin/bash
# Artifact from livecd-init
rm -f /etc/skel/.local/share/gvfs-metadata/home
[ -f /etc/lightdm/lightdm.conf ] && sed -i "/user-session=/ s/.*/user-session=lxqt/" /etc/lightdm/lightdm.conf
EOF
chmod +x /etc/anaconda-scripts.d/post-install/11-lxqt.sh
