%include pkgs-common-desktop.ks
%include pkgs-networkmanager.ks

branding-configs-fresh-desktop
chromium-browser-stable
gparted
dnfdragora-qt
simple-scan
task-plasma5-minimal
# run system-config-printer from KDE settings
# (pulls fonts-ttf-dejavu by dependencies)
plasma5-kcm-printer

# set of commonly required and more or less universal fonts
fonts-ttf-liberation
# have a font aliased to wide-spread Calibri
fonts-ttf-google-crosextra-carlito
# default font in KDE
fonts-ttf-droid
# at least one font for CJK (Chinese etc.)
fonts-otf-google-noto-sans-cjk-ttc

# preinstall translations for gtk2 apps and dialogs
gtk+2.0

# KDE developers try to migrate to pipewire
# Plasma5 Mini ISOs are experimental, so let's use pipewire
-pulseaudio
pipewire

# use sddm to avoid many dependencies of gdm
sddm
sddm-kcm
