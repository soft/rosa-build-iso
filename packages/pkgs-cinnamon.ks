#include pkgs-common-desktop.ks
#include pkgs-networkmanager.ks
branding-configs-fresh-desktop
chromium-browser-stable
cinnamon
cinnamon-control-center
cinnamon-desktop
cinnamon-menus
cinnamon-screensaver
cinnamon-session
cinnamon-settings-daemon
cinnamon-translations
dconf-editor
deluge
dnfdragora-gtk
file-roller
gdm
gnome-online-accounts
gnome-terminal
gparted
gvfs-archive
libatasmart
libgnomekbd-common
pavucontrol
rosa-elementary-theme
rosa-icons-2021-xfce-dark
rosa-icons-2021-xfce
task-iso-common
typelib(CjsPrivate)
typelib(GData)
udisks2
virtualbox-guest-additions
xed
