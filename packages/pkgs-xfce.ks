# List of packages for a maximalistic build of XFCE ISOs

# Inherit all packages from the minimalistic build
%include pkgs-xfce-mini.ks

#... and add additional packages

dnfdragora-gtk
rosa-update-system
rosa-update-system-wheel
keepassxc
libreoffice
gdm
hw-probe-pyqt5-gui

thunderbird-91
# TODO: pull it automatically if locales-ru are installed
thunderbird-91-ru

task-xfce
task-xfce-plugins

audio-recorder
blueman
gimp
kazam
meld
termhelper
transmission-gtk3

# useful for schools and life in general
kolourpaint
# editing videos is a common task for workstations (both school and office)
kdenlive
# useful screen ruler
kruler
# Programs above have already pulled KDE libraries,
# why don't we have a GUI for floppy disks...? Maybe someone will need it.
# (configurable via polkit in corporate environment)
kfloppy
rosa-imagewriter
# quickly and simply format USB sticks
# (configurable via polkit in corporate environment)
mintstick
# remote access
remmina
# Miracast
gnome-network-displays
# simply choose color (gcolor3 worked badly, TODO: retest it)
gcolor2

# additional wallpapers
rosa-wallpapers-2019

# TZ
alarm-clock-applet
runner-gui
# TODO: add entry to xfce4 settings
grub-customizer
file-roller
ksystemlog
# TODO: add gigolo to task-xfce
gigolo
onboard
# вложенные сессии
x11-server-xephyr
# TODO: explorer catfish search backends, integration with Thunar
catfish
rosa-users-quota
rosa-kiosk-gui
userdrake
