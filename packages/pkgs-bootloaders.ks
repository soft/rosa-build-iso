# Bootloaders
grub2
grub2-efi
grub2-theme-rosa
shim
# safe update for grub2 package
# copy rosa efi bootloader to default uefi path /EFI/BOOT/
grub2-install-hooks
