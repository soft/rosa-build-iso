# NetworkManager stack
# (in a separate file to allow building an ISO without NetworkManager)

networkmanager

networkmanager-fortisslvpn
networkmanager-l2tp
networkmanager-openconnect
networkmanager-openvpn
networkmanager-pptp
networkmanager-sstp
networkmanager-vpnc

# TODO: openswan, strongswan?

#networkmanager-applet # NM GUI, DE-specific
