%include pkgs-networkmanager.ks

branding-configs-fresh-server

bind-utils
chrony
openssh-server

# gnupg pulls pinentry
# pinentry-gui is pulled as a Recommended dependency of pinentry if x11-server-xorg is installed
# pinentry-qt5 will be chosen for pinentry-gui and will pull a tone of GUI packages
# They would be removed as orphans when removing Anaconda, but we do not need them in the ISO at all
-pinentry-gui

fcoe-utils
multipath-tools
# https://github.com/rhinstaller/anaconda/pull/4407
open-iscsi

# bash script for installing NVIDIA drivers
# which may be needed on servers (for CUDA)
auto-krokodil-cli

termhelper

# for ssh -X
xauth
