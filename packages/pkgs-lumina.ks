%include pkgs-networkmanager.ks

%include pkgs-common-desktop.ks
# Lumina is a minimalistic ISO without e.g. CUPS
-task-printing
-task-scanning
# Use pipewire instead of pulseaudio
# (pipewire-pulseaudio provides pulseaudio)
pipewire
-pulseaudio
pavucontrol-qt
qjackctl

task-lumina

branding-configs-fresh-desktop
chromium-browser-stable
gdm
gnome-disk-utility
gparted
networkmanager-applet
simple-scan
qpdfview

# set of commonly required and more or less universal fonts
fonts-ttf-liberation
# have a font aliased to wide-spread Calibri
fonts-ttf-google-crosextra-carlito
# at least one font for CJK (Chinese etc.)
fonts-otf-google-noto-sans-cjk-ttc

# Make XFCE be the default session in GDM
lumina-gdm

# Lumina does not run on Wayland, no sense to run
# GDM greeter via Wayland and then Lumina via Xorg
gdm-default-to-xorg

# qt4 apps use gtk2 file dialogs, pull translations
gtk+2.0
