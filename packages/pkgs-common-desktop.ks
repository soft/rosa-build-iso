# packages for all desktop ISOs

# utility for installing NVIDIA drivers
auto-krokodil-cli

openssh-clients
# It is useful to have an ssh server, sshd is disabled by default;
# Anaconda will fail to allow login by root via ssh if openshs-server is not installed
openssh-server
pdfgrep
pulseaudio
Rosa-theme-common
rosa-theme
Rosa-theme-screensaver
task-archiving
task-printing
task-scanning
task-smartcards
task-x11
virtualbox-guest-additions
xclip
wsdd
