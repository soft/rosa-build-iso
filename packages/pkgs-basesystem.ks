# packages which form base system and exist in any DE or without DE

anaconda
initial-setup
oem-install

basesystem
basesystem-minimal
basesystem-mandatory
console-gentoo-style
dnf-URPM
polkit
rosa-repos
task-kernel
linux-firmware
usb_modeswitch

systemd
# support sysv scripts
initscripts

# Filesystem programs
btrfs-progs
dosfstools
e2fsprogs
exfat-utils
f2fs-tools
hfsplus-tools
ntfs-3g
xfsprogs
lvm2
dmraid
mdadm
thin-provisioning-tools
# network filesystems
cifs-utils
nfs-utils

chrony
irqbalance
powertop

dracut

# useful programs
bind-utils
curl-gost
libressl
ima-evm-utils
ima-inspect
iputils
iptables
fio
mc
nano
nftables
netkit-telnet
openssh-clients
openssl
openssl-gost-engine
rsync
screen
squashfs-tools
sudo
tree
vim-minimal
quota
wget

# monitoring utilities
htop
iotop
lsscsi
lm_sensors
nload
hw-probe
hdparm
traceroute
mtr
ncdu
pciutils
smartmontools
usbutils

# Have at least one more or less universal font even on servers
# to avoid strange issues when rendering docs or ssh -X
fonts-ttf-freefont

# Decrypt the luks system partition without entering a password using tpm2
luksunlock

# Anaconda removes odd locales after installation
# or installs needed locales
# task-locales is used where needed
locales
locales-en
locales-ru
locales-fr
locales-de
locales-it
locales-es
locales-pt
locales-ar
locales-zh
