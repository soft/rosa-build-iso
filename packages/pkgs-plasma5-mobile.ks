%include pkgs-common-desktop.ks
%include pkgs-networkmanager.ks

branding-configs-fresh-desktop
dnfdragora-qt
task-plasma5-mobile
plasma5-mobile-config

# set of commonly required and more or less universal fonts
fonts-ttf-liberation
# have a font aliased to wide-spread Calibri
fonts-ttf-google-crosextra-carlito
# default font in KDE
fonts-ttf-droid
# at least one font for CJK (Chinese etc.)
fonts-otf-google-noto-sans-cjk-ttc

# KDE developers try to migrate to pipewire
# Plasma5 Mobile ISOs are experimental, so let's use pipewire
-pulseaudio
pipewire

# use sddm to avoid many dependencies of gdm
sddm
sddm-kcm
