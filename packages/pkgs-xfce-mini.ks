# List of package for both minimalistic and maximalistic builds of XFCE ISOs

%include pkgs-common-desktop.ks
%include pkgs-networkmanager.ks

branding-configs-fresh-desktop
chromium-browser-stable
gdm
gnome-disk-utility
gparted
simple-scan
task-xfce-minimal

# set of commonly required and more or less universal fonts
fonts-ttf-liberation
# have a font aliased to wide-spread Calibri
fonts-ttf-google-crosextra-carlito
# at least one font for CJK (Chinese etc.)
fonts-otf-google-noto-sans-cjk-ttc

# Make XFCE be the default session in GDM
rosa-xfce-config-gdm

# XFCE does not run on Wayland, no sense to run
# GDM greeter via Wayland and then XFCE via Xorg
gdm-default-to-xorg

# qt4 apps use gtk2 file dialogs, pull translations
gtk+2.0
