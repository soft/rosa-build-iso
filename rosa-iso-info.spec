# The idea is to provide metadata how user's system was installed via RPM.
# Example usage in dependencies of other packages:
# Recommends: (foo if rosa-iso-info(buildid) >= 555)
# Recommends: (foo if rosa-iso-info >= 202110-555)
# Recommends: (foo if rosa-iso-info < 202220)

# rpmbuild \
# 	--define "rosa_platform 202110" \
#	--define "iso_build_id $BUILD_ID" \
#	--define "iso_build_time $(date --utc +%s)" \
#	--define "iso_de server"
#	--define "iso_comment $COMMENT"

%define iso_de_lowercase %(echo %{iso_de} | tr '[[:upper:]]' '[[:lower:]]')
%define _build_pkgcheck_set %{nil}
%define _build_pkgcheck_srpm %{nil}

Name: rosa-iso-info
Summary: Information about which ISO image this OS was installed from
License: Public Domain
Group: System/Base
# e.g.: 202110, an integer to make > and < work if needed
Version: %{rosa_platform}
# e.g.: 258963
Release: %{iso_build_id}
Provides: rosa-iso-info(platform) = %{rosa_platform}
Provides: rosa-iso-info(buildid) = %{iso_build_id}
# UNIX time stamp in UTC+0 (date --utc +%s)
Provides: rosa-iso-info(buildtime) = %{iso_build_time}
# server, gnome, plasma5, lxqt, xfce, mate etc., in lowercase
Provides: rosa-iso-info(de) = %{iso_de_lowercase}
BuildArch: noarch
AutoProv: no
AutoReq: no

%description
%{iso_comment}

%files
/var/lib/rosa-iso-info

#---------------------------------------------------------------------

%prep

%build

%install
mkdir -p %{buildroot}/var/lib
cat > %{buildroot}/var/lib/rosa-iso-info << EOF
BUILD_ID=%{iso_build_id}
BUILD_TIME=%{iso_build_time}
DE=%{iso_de_lowercase}
PLATFORM=%{rosa_platform}
EOF
