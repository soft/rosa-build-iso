auth --useshadow --enablemd5
selinux --disabled
firewall --disabled
firstboot --enabled

# Enable/Disable services
# If some services from this list do not exist, they are skipped
services --enabled=gdm,avahi-daemon,cpupower,irqbalance,dbus,cups,smb,wsdd,systemd-resolved,NetworkManager

timezone --utc Europe/Moscow

lang en_US.UTF-8 --addsupport=ru_RU.UTF-8,fr_FR.UTF-8,it_IT.UTF-8,pt_BR.UTF-8,es_ES.UTF-8,de_DE.UTF-8

# keyboard --xlayouts=us,ru --switch=grp:alt_shift_toggle

%include DEs/@DE@/head.ks
%include customizations/head.ks

%include repos/repos-@ARCH@.ks
%include @ADD_REPOS@

%packages --nocore
%include packages/pkgs-basesystem.ks
%include packages/pkgs-bootloaders.ks
%include packages/pkgs-vm.ks
%include packages/pkgs-@DE@.ks
%include customizations/packages.ks
%include @ADD_PACKAGES@
%end

%post
### Manually (re)generate initrds
# TODO: move its generation from %%post to %%postrrans in kernel
# packages, and then all needed dracut modules will be already
# installed during inird generation, and there will be no need
# to (re)generate initrds here.
# BUT dracut config by livecd-tools is partly broken
# https://github.com/livecd-tools/livecd-tools/issues/158
# We remove odd parts - modules that do not exist in this kernel.
# Another possible approach is making and using our own config from scratch.
C="${C:-/etc/dracut.conf.d/99-liveos.conf}"

find /boot -name 'vmlinuz-*' | sort -u | while read -r line ; do
	kver="$(echo "$line" | sed -e 's,^/boot/vmlinuz-,,g')"
	cp "${C}" "${C}.orig"
	to_find="$(cat "$C" | grep -E '^(filesystems\+=|add_drivers\+=)' | sed -e 's,",,g' | awk -F '+=' '{print $NF}' | tr ' ' '\n' | grep -v '=' | tr '\n' ' ' | sed -e 's,  , ,g')"
	not_exist_list=""
	for i in ${to_find} ; do
		if ! find "/lib/modules/${kver}" -name "${i}.ko*" | grep -q '.' ; then
			not_exist_list="${not_exist_list} ${i}"
		fi
	done
	sed -i -e 's,+=",+=" ,g' "$C"
	for i in ${not_exist_list} ; do
		sed -i -E -e "s,[[:blank:]]${i}[[:blank:]], ,g" "$C"
	done
	sed -i -e 's,  , ,g' "$C"
	diff -u "${C}.orig" "${C}" || :
	rm -f "${C}.orig"
	dracut -f "/boot/initrd-${kver}.img" "${kver}"
done
find /boot -name 'initrd-*' -print

# Pre-import RPM keys to avoid questions from DNF if installing/updating packages in LiveCD
if rpm -q --whatprovides rosa-repos-keys >/dev/null 2>&1
then
	rpm -ql --whatprovides rosa-repos-keys | grep '^/etc/pki/rpm-gpg/' | while read -r line
	do
		rpm --import "$line"
	done
else
	echo "rosa-repos-keys is not installed, skipping import of GPG keys"
fi

# Make "ConditionNeedsUpdate=|/etc" in ldconfig.service be false
# systemd-update-done.service(8) is not needed in LiveCDs
touch /etc/.updated /var/.updated

%include @ADD_REPOS_INTO_ISO@
%include DEs/@DE@/post.ks
%include customizations/post.ks
%end

%post --nochroot
# also used in customizations/
export RESULTS_DIR="@RESULTS_DIR@"
export BUILD_ID="@BUILD_ID@"
export DIR0="@DIR0@"
rpm --root "$INSTALL_ROOT" -iv "@RPMBUILD_TMP@/rosa-iso-info.rpm"
rpm --root "$INSTALL_ROOT" -qa --queryformat="%{SOURCERPM}\n" | rev | cut -d '-' -f 3- | rev | sort -u > @RESULTS_DIR@/srpms.@DE@_@BUILD_ID@.log
rpm --root "$INSTALL_ROOT" -qa --queryformat="%{NAME} %{ARCH} %{VERSION} %{RELEASE} %{SOURCERPM} %{BUILDTIME}\n" | sort -u > @RESULTS_DIR@/rpms.@DE@_@BUILD_ID@.log
rpm --root "$INSTALL_ROOT" -qa --dump | sort -u | xz -9 -T0 > @RESULTS_DIR@/dump.@DE@_@BUILD_ID@.txt.xz
%include customizations/post-nochroot.ks
%end
